const btn1 = document.getElementById('btn1');
const btn2 = document.getElementById('btn2');
const status = document.getElementById('status');
const pic = document.getElementById('img');

btn1.addEventListener("click", function() {
  pic.src = "img/pic_bulbon.gif";
  status.style.color = "green";
  status.innerHTML = "the Light Status is : On";
  pic.style.border ="2px solid #000";
});

btn2.addEventListener("click", function() {
  pic.src = "img/pic_bulboff.gif";
  status.style.color = "red";
  status.innerHTML = "the Light Status is : Off";
  pic.style.border ="1px solid #eee";
})
