import React from 'react'
import Loading from './Loading'
import Tours from './tours'

function App() {
  return (
    <div className="App">
      <h1>Test</h1>
      <Tours />
    </div>
  );
}

export default App;
