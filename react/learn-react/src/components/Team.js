import React from 'react';

import TeamMemeber from './TeamMember'
function Team() {
    
    return (
        <div className="row">
            <TeamMemeber 
                info={
                    {
                        img: './images/1.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                } 
            />
            <TeamMemeber
                info={
                    {
                        img: './images/2.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/3.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/4.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/5.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/6.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/7.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/8.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />
            <TeamMemeber
                info={
                    {
                        img: './images/9.jpg',
                        name: 'Walter White',
                        position: "Boos",
                        email: 'Walter@company.com',
                        website: 'walter.com',
                    }
                }
            />

        </div>
    )
}
export default Team