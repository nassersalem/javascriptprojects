import React from 'react'

function TeamMemebr(props){

    return(
        <div className="col-md-4 col-sm-6">
            <div className="card">
                <div className="card-header">
                    <img src={props.info.img} alt={props.info.name}/>
                </div>
                <div className="card-body">
                    <h3>{`Name: ${props.info.name}`}</h3>
                    <h4>{`Position: ${props.info.position}`}</h4>
                    <div>{`Phone: ${props.info.phone}`}</div>
                    <div>{`Email: ${props.info.email}`}</div>
                    <div>{`Website: ${props.info.website}`}</div>
                </div>
            </div>
        </div>
    )
}

export default TeamMemebr