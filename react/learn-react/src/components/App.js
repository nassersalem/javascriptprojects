import React from 'react';
import Header from './Header'
import Team from './Team'
import Myfooter from './Myfooter'

function App(){
    

    return(
        <div className="container">
            <Header />
            <Team />
            <Myfooter />
        </div>
    )
}
export default App